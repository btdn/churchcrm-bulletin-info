(in-package :cl-csv)
(setf *escape-mode* :following)

(in-package :churchcrm)

(declaim ((integer 0 *)
	  *birthday-calendar-id*
	  *birthday-title-prefix-length*))

(defparameter *birthday-calendar-id* 0)
(defparameter *birthday-title-prefix-length* (length "Birthday: "))

(defstruct connection-info
  (base-url nil :type string)
  (api-key nil :type string))

(defstruct deposit
  (id 0 :type (integer 0 *))
  (date 0 :type (integer 0 *))
  (comment "" :type string)
  (total-amount 0 :type rational))

(defstruct pledge
  (id 0 :type (integer 0 *))
  (amount 0 :type rational)
  (donation-fund-name "" :type string))

(defstruct birthday
  (id 0 :type (integer 0 *))
  (name "" :type string)
  (date 0 :type (integer 0 *)))

(push (cons "application" "json") drakma:*text-content-types*)
(setf yason:*parse-json-arrays-as-vectors* T)

(defun api-key-header (connection-info)
  (declare (connection-info connection-info))
  "Internal function: get additional-headers alist for requests."
  (cons :x-api-key (connection-info-api-key connection-info)))

(defun get-url (connection-info path-list &optional arg-list)
  (declare (connection-info connection-info)
	   (cons path-list))
  "Internal function: using the Base-url, add the path."
  (format nil
	  "~A~{~A~^/~}?~{~A=~A~^&~}"
	  (connection-info-base-url connection-info)
	  path-list
	  arg-list))

(defun request (connection-info path &optional args)
    (declare (connection-info connection-info)
	     (cons path))
    "Internal function: make HTTP request and return response"
    (let ((url (get-url connection-info path args))
	  (headers (list (api-key-header connection-info))))
      (drakma:http-request url :additional-headers headers)))

(defun table-to-deposit (table)
  (declare (hash-table table))
  "Internal function."
  (let ((totalAmount (gethash "totalAmount" table "0.00")))
    (when (equal totalAmount nil)
      ;; Cludgy workaround for when the deposit does not have any payments
      (setf totalAmount "0.00"))
    (make-deposit :id (gethash "Id" table)
		  :date (parse-date-time (gethash "Date" table))
		  :comment (gethash "Comment" table)
		  :total-amount (parse-decimal-number totalAmount))))

(defun table-to-pledge (table)
  (declare (hash-table table))
  "Internal function."
  (make-pledge :id (gethash "Id" table)
	       :amount (parse-decimal-number (gethash "Amount" table))
	       :donation-fund-name (gethash "DonationFundName" table)))

(defun request-deposits  (connection-info)
  (declare (connection-info connection-info))
  (request connection-info '("api" "deposits")))

(defun get-deposits (connection-info)
  (declare (connection-info connection-info))
  "Gets the deposits."
  (let* ((deposit-tables (gethash "Deposits"
				  (yason:parse (request-deposits connection-info))))
	 (deposits (make-array (length deposit-tables)
			       :initial-element (make-deposit)
			       :element-type 'deposit)))
    (dotimes (i (length deposit-tables))
      (setf (aref deposits i)
	    (table-to-deposit (aref deposit-tables i))))
    deposits))

(defun request-deposit-csv (connection-info deposit-id)
  (declare (connection-info connection-info)
	   ((integer 1 *) deposit-id))
  (request connection-info
	   (list "api"
		 "deposits"
		 (write-to-string deposit-id)
		 "csv")))

(defun get-deposit-fund-totals (connection-info deposit-id)
  (declare (connection-info connection-info)
	   ((integer 1 *) deposit-id))
  (let* ((fund-totals (make-hash-table :test #'equalp))
	 (deposit-csv (read-csv (request-deposit-csv connection-info deposit-id)))
	 (amount-position (position "Amount"
				    (first deposit-csv)
				    :test #'equalp))
	 (fund-position (position "DonationFundName"
				  (first deposit-csv)
				  :test #'equalp)))
    (dolist (payment (rest deposit-csv))
      (incf (gethash (nth fund-position payment) fund-totals 0)
	    (parse-decimal-number (nth amount-position payment))))
    fund-totals))

(defun table-to-birthday (table)
  (declare (hash-table table))
  "Internal function."
  (make-birthday :id (gethash "id" table)
		 :name (subseq (gethash "title" table) *birthday-title-prefix-length*)
		 :date (parse-date-time (gethash "start" table))))

(defun request-birthdays (connection-info start end)
  (declare (connection-info connection-info)
	   ((integer 0 *) start end))
  (assert (<= start end))
  (request connection-info
	   (list "api"
		 "systemcalendars"
		 (write-to-string *birthday-calendar-id*)
		 "fullcalendar")
	   ;; as of ChurchCRM Version 3.0.5, start and end are ignored
	   (list "start" ""
		 "end" "")))

(defun is-date-in-between-fun (start end)
  (declare ((integer 0 *) start end))
  #'(lambda (date)
      (declare ((integer 0 *) date))
      (and (<= date end)
	   (>= date start))))

(defun sort-birthday-date (birthdays)
  (declare ((vector birthday) birthdays))
  (sort birthdays #'< :key #'birthday-date))

(defun tables-to-birthdays (tables)
  (declare ((vector hash-table) tables))
  (map 'vector #'table-to-birthday tables))

(defun table-get-date (table)
  (declare (hash-table table))
  (parse-date-time (gethash "start" table)))

(defun delete-if-date-not-between (tables start end)
  (declare ((vector hash-table) tables)
	   ((integer 0 *) start end))
  (assert (<= start end))
  ;;; since, as of ChurchCRM Version 3.0.5, the server does not filter for us, need to do so here
  (delete-if-not (is-date-in-between-fun start end)
		 tables
		 :key #'table-get-date))

(defun get-birthdays (connection-info start end)
  (declare (connection-info connection-info)
	   ((integer 0 *) start end))
  "Get the birthdays in the date range."
  (assert (<= start end))
  (let ((tables (parse (request-birthdays connection-info start end))))
    (sort-birthday-date (tables-to-birthdays (delete-if-date-not-between tables
									 start
									 end)))))
