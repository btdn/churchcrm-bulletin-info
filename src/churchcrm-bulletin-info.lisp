(in-package :churchcrm-bulletin-info)

(defconstant +seconds-per-day+ 86400)
(defconstant +days-per-week+ 7)

(defun days-math (fun time days)
  (declare (function fun)
	   ((integer 0 *) time days))
  (funcall fun time (* +seconds-per-day+ days)))

(defun days-subtract (time days)
  (declare ((integer 0 *) time days))
  (days-math #'- time days))

(defun days-add (time days)
  (declare ((integer 0 *) time days))
  (days-math #'+ time days))

(defun is-time-in-range (time start end)
  (declare ((integer 0 *) time start end))
  (assert (<= start end))
  (and (>= time start)
       (<= time end)))

(defun merge-fund-totals (table1 table2)
  (declare (hash-table table1 table2))
  "Merge Table2 into Table1."
  (loop for fund being the hash-keys of table2
     do (setf fund (string-downcase fund))
       (incf (gethash fund table1 0) (gethash (string-downcase fund) table2))))

(defun get-deposits-in-date-range (connection-info start-time end-time)
  (declare (connection-info connection-info)
	   ((integer 0 *) start-time end-time))
  (let ((fund-totals (make-hash-table :test #'equalp))
	(included-deposits ()))
    (loop
       for deposit
       across (get-deposits connection-info)
       do
	 (when (is-time-in-range (deposit-date deposit) start-time end-time)
	   (push deposit included-deposits)
	   (merge-fund-totals fund-totals
			      (get-deposit-fund-totals connection-info
						       (deposit-id deposit)))))
    (values (alexandria:hash-table-plist fund-totals)
	    included-deposits)))

(defun format-deposit (deposit)
  (declare (type (or null cons) deposit))
  (format nil "~{~A=$~$~^; ~}" deposit))

(defun format-birthday (stream birthday)
  (declare (stream stream)
	   (birthday birthday))
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time (birthday-date birthday))
    (declare (ignore second minute hour year))
    (format stream
	    "~A (~A/~A)"
	    (birthday-name birthday)
	    month
	    date)))

(defun bulletin-stewardship (connection-info year-month-day)
  (declare (connection-info connection-info)
	   (string year-month-day))
  "Get the formatted Deposits for the 7 days through Year-Month-Day."
  (let ((date (parse-date-time year-month-day)))
    (multiple-value-bind (fund-totals deposits-used)
	(get-deposits-in-date-range connection-info
				    (days-subtract date +days-per-week+)
				    date)
      (values (format-deposit fund-totals) deposits-used))))

(defun bulletin-birthdays (connection-info year-month-day)
  (declare (connection-info connection-info)
	   (string year-month-day))
  "Get the birthdays for the 7 days starting on Year-Month-Day."
  (let ((date (parse-date-time year-month-day)))
    (format nil
	    "~{~A~^, ~}"
	    (coerce (get-birthdays connection-info date (days-add date +days-per-week+))
		    'list))))

(defun bulletin (connection-info year-month-day)
  (declare (connection-info connection-info)
	   (string year-month-day))
  (values (multiple-value-call #'bulletin-stewardship connection-info year-month-day)
	  (bulletin-birthdays connection-info year-month-day)))
