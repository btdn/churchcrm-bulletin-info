(defpackage churchcrm
  (:use :cl)
  (:import-from :cl-csv		:read-csv)
  (:import-from :cl-date-time-parser
		:parse-date-time)
  (:import-from :decimals
		:parse-decimal-number)
  (:import-from :drakma)
  (:import-from :yason
		:parse)
  (:export birthday-calendar-id
	   
	   connection-info
	   make-connection-info
	   connection-info-base-url
	   connection-info-api-key

	   deposit
	   deposit-id
	   deposit-date

	   birthday
	   birthday-id
	   birthday-name
	   birthday-date

	   get-deposits

	   get-deposit-fund-totals

	   get-birthdays))

(defpackage churchcrm-bulletin-info
  (:use :cl
	:churchcrm)
  (:import-from :cl-date-time-parser
		:parse-date-time)
  (:export bulletin-birthdays
	   bulletin-stewardship
	   bulletin

	   format-birthday
	   format-deposit))
