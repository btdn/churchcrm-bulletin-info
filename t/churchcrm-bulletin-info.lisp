(in-package :churchcrm-bulletin-info-tests)

(def-suite churchcrm-bulletin-info)

(in-suite churchcrm-bulletin-info)

(defun all-tests ()
  (run! 'churchcrm-bulletin-info))

(defun test-format-deposit (deposit expected-output)
  (is (string= expected-output
	       (churchcrm-bulletin-info::format-deposit deposit))))

(test format-deposit-whole-number
  (test-format-deposit (list "fund" (rational 10))
			"fund=$10.00"))

(test format-deposit-with-decimal-zeros
  (test-format-deposit (list "fund" (rational 10.00))
			"fund=$10.00"))

(test format-deposit-with-decimal-cents
  (test-format-deposit (list "fund" (rational 10.50))
			"fund=$10.50"))

(test format-deposit-multiple-funds
  (test-format-deposit (list "fund" (rational 10.50)
			      "fund1" (rational 10.01))
			"fund=$10.50; fund1=$10.01"))

(test format-deposit-nil
  (test-format-deposit nil
		       ""))
