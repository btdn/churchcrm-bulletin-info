;#!/usr/bin/sbcl --script

;;; load quicklisp and dependencies

(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload "churchcrm-bulletin-info"
	      :silent t)

(defpackage churchcrm-bulletin-info-stgeorge
  (:use :cl :churchcrm :churchcrm-bulletin-info))

(in-package :churchcrm-bulletin-info-stgeorge)

;;; handle command-line arguments

(defvar *args*
  (uiop:command-line-arguments)
  "Map implementation-dependent variable for command-line arguments.")

(unless (eq 3 (length *args*))
  (format *error-output* "usage: bulletin-info base-url api-key year-month-day~%")
  (uiop:quit 1))

(declaim (type connection-info *connection-info*)
	 (type string *year-month-day*))

(defvar *connection-info*
  (make-connection-info :base-url (first *args*)
			:api-key (second *args*))
  "Base-URL and Api-key.")

(defvar *year-month-day*
  (third *args*)
  "The date as a string: YYYY-MM-DD.")

;;; helper functions for output

(set-pprint-dispatch 'birthday #'format-birthday)

(defun print-hr ()
  (princ #\linefeed)
  (princ "------------------------------------------------------------------")
  (princ #\linefeed))

(defun print-header (&rest args)
  (princ #\linefeed)
  (dolist (arg args)
    (princ arg))
  (princ #\linefeed))

(defun print-text (&rest args)
  (dolist (arg args)
    (princ arg)))

(defun format-deposits (deposits)
  (format nil "~{~S~^~%~}" deposits))

;;; functions for printing interesting hings

(defun print-stewardship ()
  (print-header "Stewardship:")
  (multiple-value-bind (text deposits)
      (bulletin-stewardship *connection-info* *year-month-day*)
    (declare (string text)
	     (type (or null cons) deposits))
    (cond (deposits (print-text #\newline text #\newline)
		    (print-header "Included deposits (for debugging):")
		    (print-text #\newline (format-deposits deposits)) #\newline)
	  (t (print-text #\newline "No deposits in date-range." #\newline)))))

(defun print-birthdays ()
  (print-header "Birthdays:")
  (let ((birthdays (bulletin-birthdays *connection-info* *year-month-day*)))
    (print-text (cond ((not (equal birthdays "")) birthdays)
		      (t "No birthdays in date-range."))
		#\linefeed)))
;;; print things

(print-header "Bulletin information for " *year-month-day*)

(print-hr)

(print-stewardship)

(print-hr)

(print-birthdays)
