Option Explicit

Private Const SCHEDULE_NAME As String = "Name"
Private Const PROP_ICON_TITLE As String = "Icon Title"
Private Const PROP_BIRTHDAYS As String = "Birthdays"
Private Const PROP_INTENTION As String = "Intention"
Private Const PROP_PROKIMENON As String = "Prokimenon"
Private Const PROP_PROKIMENON_STICH As String = "Prokimenon - Stichon"
Private Const PROP_READER As String = "Reader"
Private Const PROP_READING_APOSTOLIC As String = "Reading - Apostolic"
Private Const PROP_ALLELUIA As String = "Alleluia"
Private Const PROP_ALLELUIA_STICH  As String = "Alleluia - Stichon"
Private Const PROP_READING_GOSPEL As String = "Reading - Gospel"
Private Const PROP_STEWARDSHIP As String = "Stewardship"
Private Const PROP_USHERS As String = "Ushers"
Private Const PROP_RESURRECTIONAL_TONE As String = "Resurrection Tone"
Private Const PROP_RESURRECTION_TEXT As String = "Resurrection Troparion Text"

Private Const PROP_READER_NEXT As String = "Reader - Next Week"
Private Const PROP_USHERS_NEXT As String = "Ushers - Next Week"
Private Const PROP_NEXT_WEEK As String = "Next Week"

Private Const PROP_ECB_TITLE As String = "ECB Title"
Private Const SECTION_ECB_TEXT As String = "ECB Text"

Private Const DATABASE_NAME As String = "BulletinInfo"
Private Const DATABASE_SCHEDULE_QUERY_NAME As String = "GetSchedule"
Private Const DATABASE_INTENTION_QUERY_NAME As String = "GetStoleOfferings"
Private Const DATABASE_OCTOECHOS_QUERY_NAME As String = "GetOctoechos"

Private Const DATABASE_STOLE_REQUEST = "Requested For"
Private Const DATABASE_STOLE_REQUESTER = "Requested By"

Private Sub DeleteTextFromSection
	Dim TextSections As Object
	Dim SelectionsToRemoveTextFrom() As Variant
	Dim sectionName As String
	Dim section As Object

	TextSections = ThisComponent.TextSections

	SelectionsToRemoveTextFrom = Array(SECTION_ECB_TEXT)

	For Each sectionName In SelectionsToRemoveTextFrom
		If TextSections.hasByName(sectionName) Then
			section = TextSections.getByName(sectionName).getChildSections()
			ThisComponent.CurrentController.select(section.Anchor.End)
		EndIf
	Next
End Sub

Private Sub DeleteOldProperties
	Dim UDP As Object
	Dim PropertiesToRemove() As Variant
	Dim prop As String

	UDP = ThisComponent.DocumentProperties.UserDefinedProperties

	PropertiesToRemove = Array(PROP_ICON_TITLE,_
		PROP_BIRTHDAYS, _
		PROP_INTENTION, _
		PROP_PROKIMENON, _
		PROP_PROKIMENON_STICH,_
		PROP_ALLELUIA,_
		PROP_ALLELUIA_STICH,_
		PROP_STEWARDSHIP,_
		PROP_ECB_TITLE,_
		PROP_RESURRECTION_TEXT)

	For Each prop In PropertiesToRemove
		If UDP.getPropertySetInfo.hasPropertyByName(prop) Then
			UDP.setPropertyValue(prop, "")
		EndIf
	Next
End Sub

Private Sub MoveProperties
	Dim UDP As Object
	Dim PropertiesToMove() As Variant
	Dim propPair() As Variant

	UDP = ThisComponent.DocumentProperties.UserDefinedProperties

	PropertiesToMove = Array(Array(PROP_READER_NEXT, PROP_READER), _
		Array(PROP_USHERS_NEXT, PROP_USHERS))

	For Each propPair in PropertiesToMove
		UDP.setPropertyValue(propPair(1), UDP.getPropertyValue(propPair(0)))
		UDP.setPropertyValue(propPair(0), "")
	Next
End Sub

Function GetNewTitle(d As Date) As String
	Dim UDP As Object
	Dim formatedDate As String
	Dim thisWeek As String
	Dim title As String

	UDP = ThisComponent.DocumentProperties.UserDefinedProperties

	formatedDate = BulletinUtilities.FormatDate(d)
	thisWeek = UDP.getPropertyValue(PROP_NEXT_WEEK)

	title = formatedDate & " — " & thisWeek

	GetNewTitle=title
End Function

Private Sub UpdateTitle(d As Date)
	Dim newTitle As String
	Dim DP As Object
	Dim UDP As Object

	DP = ThisComponent.DocumentProperties
	UDP = DP.UserDefinedProperties

	newTitle = GetNewTitle(d)

	DP.title = newTitle
	UDP.setPropertyValue(PROP_NEXT_WEEK, "")
End Sub

Private Sub SetPropertyFromQueryResult(queryResult As Object, sourceColumn As String, destProperty As String)
	Dim UDP As Object
	Dim column As Long
	Dim value As Variant

	UDP = ThisComponent.DocumentProperties.UserDefinedProperties

	If Not UDP.getPropertySetInfo.hasPropertyByName(destProperty) Then
		MsgBox("Property '" & destProperty & "' does not exist.")
		Exit Sub
	EndIf

	column = queryResult.findColumn(sourceColumn)

	If StrComp(sourceColumn, PROP_RESURRECTIONAL_TONE)=0 Then
		value = queryResult.getDouble(column)
	Else
		value = queryResult.getString(column)
	EndIf

	UDP.setPropertyValue(destProperty, value)
End Sub

Private Sub UpdateFromSchedule(dataSource As Object, connection As object, d As Date)
	Dim query As String
	Dim sqlStatement As Object
	Dim queryResult As Object
	Dim nextWeekDate As Date

	query = BulletinUtilities.GetQueryCommand(dataSource, DATABASE_SCHEDULE_QUERY_NAME)

	rem get information about this week
	sqlStatement = connection.prepareStatement(query)
	sqlStatement.setDate(1, BulletinUtilities.GetUnoDate(d))

	queryResult = sqlStatement.executeQuery
	If queryResult.next Then
		SetPropertyFromQueryResult(queryResult, PROP_READING_APOSTOLIC, PROP_READING_APOSTOLIC)
		SetPropertyFromQueryResult(queryResult, PROP_READING_GOSPEL, PROP_READING_GOSPEL)
		SetPropertyFromQueryResult(queryResult, PROP_RESURRECTIONAL_TONE, PROP_RESURRECTIONAL_TONE)

		rem get information about next week
		nextWeekDate = DateAdd("d", 7, d)
		sqlStatement = connection.prepareStatement(query)
		sqlStatement.setDate(1, GetUnoDate(nextWeekDate))

		queryResult = sqlStatement.executeQuery
		If queryResult.next Then
			SetPropertyFromQueryResult(queryResult, PROP_READER, PROP_READER_NEXT)
			SetPropertyFromQueryResult(queryResult, PROP_USHERS, PROP_USHERS_NEXT)
			SetPropertyFromQueryResult(queryResult, SCHEDULE_NAME, PROP_NEXT_WEEK)
		Else
			MsgBox("No schedule found for " & nextWeekDate & ".")
		EndIf
	Else
		MsgBox("No schedule found for " & d & ".")
	EndIf
End Sub

Private Sub UpdateFromOctoechosByTone(dataSource As Object, connection As Object, tone As Integer)
	Dim query As String
	Dim sqlStatement As Object
	Dim queryResult As Object
	
	query = BulletinUtilities.GetQueryCommand(dataSource, DATABASE_OCTOECHOS_QUERY_NAME)

	sqlStatement = connection.prepareStatement(query)
	sqlStatement.setShort(1, tone)

	queryResult = sqlStatement.executeQuery
	If queryResult.next Then
		SetPropertyFromQueryResult(queryResult, "Troparion", PROP_RESURRECTION_TEXT)
		SetPropertyFromQueryResult(queryResult, PROP_PROKIMENON, PROP_PROKIMENON)
		SetPropertyFromQueryResult(queryResult, "ProkimenonStich", PROP_PROKIMENON_STICH)
	Else
		MsgBox("No information found for tone " & d & ".")
	EndIf
End Sub

Private Sub UpdateFromOctoechos(dataSource As Object, connection As Object)
 	Dim UDP As Object
 	Dim toneString As String
 	Dim tone As Integer

	UDP = ThisComponent.DocumentProperties.UserDefinedProperties
	
	toneString = UDP.getPropertyValue(PROP_RESURRECTIONAL_TONE)
 	tone = Cint(toneString)
 	
 	UpdateFromOctoechosByTone(dataSource, connection, tone)
End Sub

Private Function GetStoleOfferings(dataSource As Object, connection As Object, d As Date) As String
	Dim query As String
	Dim sqlStatement As Object
	Dim queryResult As Object

	Dim stoleOfferings As String
	Dim request As String
	Dim requester As String

	query = BulletinUtilities.GetQueryCommand(dataSource, DATABASE_INTENTION_QUERY_NAME)

	sqlStatement = connection.prepareStatement(query)
	sqlStatement.setDate(1, GetUnoDate(d))

	stoleOfferings = ""
	queryResult = sqlStatement.executeQuery
	While queryResult.Next
		If Len(stoleOfferings)>0 Then
			stoleOfferings = stoleOfferings & "; "
		EndIf

		request = queryResult.getString(queryResult.findColumn(DATABASE_STOLE_REQUEST))
		requester = queryResult.getString(queryResult.findColumn(DATABASE_STOLE_REQUESTER))

		stoleOfferings = stoleOfferings & request & " (req. by " & requester & ")"
	Wend

	GetStoleOfferings=stoleOfferings
End Function

Private Sub UpdateStoleOfferings(stoleOfferings As String)
	Dim UDP As Object

	UDP = ThisComponent.DocumentProperties.UserDefinedProperties

	If Len(stoleOfferings)<1 Then
		MsgBox("No intentions found.")
	Else
		UDP.setPropertyValue(PROP_INTENTION, stoleOfferings)
	EndIf
End Sub

Private Sub UpdateFromDatabase(d As Date)
	Dim databaseContext As Object
	Dim dataSource As Object
	Dim connection As Object

	databaseContext = createUnoService("com.sun.star.sdb.DatabaseContext")
	dataSource = databaseContext.getByName(DATABASE_NAME)
	connection = datasource.GetConnection("","")

	UpdateFromSchedule(dataSource, connection, d)
	UpdateStoleOfferings(GetStoleOfferings(dataSource, connection, d))
	UpdateFromOctoechos(dataSource, connection)
End Sub

Private Sub DatabaseOnlyUpdate()
	UpdateFromDatabase(BulletinUtilities.GetDateFromFileName())
End Sub

Private Sub DeleteTextFromSections()
	Dim sectionsToClear() As Variant
	Dim section As String

	sectionsToClear = Array("GospelReading", _
		"ApostolicReading", _
		"ECBText")

	For Each section in sectionsToClear
		BulletinUtilities.DeleteTextFromSection(section)
	Next
End Sub

Public Sub UpdateStoleOfferingsAutomatic()
	Dim d As Date
	Dim databaseContext As Object
	Dim dataSource As Object
	Dim connection As Object

	databaseContext = createUnoService("com.sun.star.sdb.DatabaseContext")
	dataSource = databaseContext.getByName(DATABASE_NAME)
	connection = datasource.GetConnection("","")
	
	UpdateStoleOfferings(GetStoleOfferings(dataSource, connection, d))
End Sub

Public Sub Main
	Dim d As Date

	d = BulletinUtilities.GetDateFromFileName()

	DeleteOldProperties()
	DeleteTextFromSections()
	MoveProperties()
	UpdateTitle(d)

	UpdateFromDatabase(d)
End Sub
