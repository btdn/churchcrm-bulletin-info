﻿Option Explicit

Public Function GetQueryCommand(dataSource As Object, queryName as String) As String
	GetQueryCommand=dataSource.getQueryDefinitions().getByName(queryName).Command
End Function

Public Function GetDateFromFileName As Date
	Dim fileName As String
	Dim fileDate As Date

	GlobalScope.BasicLibraries.loadLibrary("Tools")

	fileName = Tools.Strings.FileNameoutofPath(ThisComponent.getURL())
	rem could do error checking here...
	fileDate = Tools.ReplaceString(fileName, "", "_Bulletin.odt")

	GetDateFromFileName=fileDate
End Function

Public Function GetUnoDate(d As Date) As Object
	Dim unoDate As Object

	unoDate = createUnoStruct("com.sun.star.util.Date")
	unoDate.Year = Year(d)
	unoDate.Month = Month(d)
	unoDate.Day = Day(d)

	GetUnoDate=unoDate
End Function

Public Function FormatDate(d As Date) As String
	FormatDate=Format(d, "[$-409]MMMM D, YYYY")
End Function

Public Sub DeleteTextFromSection(sectionName As String)
	Dim section As Object
	Dim cursor As Object
	Dim txt As Object
	
	
	If ThisComponent.TextSections.hasByName(sectionName) Then
		section = ThisComponent.TextSections.getByName(sectionName)
		cursor = ThisComponent.getText().createTextCursor()
		cursor.gotoRange(section.Anchor.Start, false)
		cursor.gotoRange(section.Anchor.End, true)
		txt = cursor.getText()
	
		txt.insertString(cursor, "", true)
	Else
		MsgBox("Did not find TextSection with name " & sectionName & ".")
	End If
End Sub

