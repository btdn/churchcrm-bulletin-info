(load "/usr/lib/sbcl/contrib/uiop.fasl")

(defpackage "BULLETIN-WEEK-NAME"
  (:use "COMMON-LISP")
  (:import-from "UIOP/UTILITY" "STRING-PREFIX-P" "SPLIT-STRING")
  (:export "SHORT-NAME-TO-FULL-NAME"))

(in-package "BULLETIN-WEEK-NAME")

(defun get-number-from-short-name (short-name)
  (parse-integer (first (last (split-string short-name)))))

(defun format-full-name (format short-name)
  (format nil format (get-number-from-short-name short-name)))
 
(defun short-name-to-full-name (short-name)
  (cond ((string-prefix-p "Cross " short-name) (format-full-name "~:(~:R~) Sunday after the Holy Cross" short-name))
	((string-prefix-p "Pent " short-name) (format-full-name "~:(~:R~) Sunday after Pentecost" short-name))
	((string-prefix-p "Lent " short-name) (format-full-name "~:(~:R~) Sunday of Lent" short-name))
	((string-prefix-p "Easter " short-name) (format-full-name "~:(~:R~) Sunday of Easter" short-name))
	(t short-name)))

(let ((character-bag '(#\Space #\Tab #\Newline)))
  (loop for line = (read-line *standard-input* nil :eof)
     until (eq line :eof)
     do
       (format t "~A~%" (short-name-to-full-name (string-trim character-bag line)))))
