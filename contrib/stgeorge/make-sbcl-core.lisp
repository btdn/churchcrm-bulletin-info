(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))
(ql:quickload "churchcrm-bulletin-info")

(save-lisp-and-die "churchcrm-bulletin-info" :executable T :purify T)
