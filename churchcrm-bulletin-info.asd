(defsystem "churchcrm-bulletin-info"
  :author "Benjamin Newman"
  :depends-on ("cl-csv"
	       "cl-date-time-parser"
	       "decimals"
	       "drakma"
	       "yason")
  :serial t
  :components ((:file "src/packages")
	       (:file "src/churchcrm")
	       (:file "src/churchcrm-bulletin-info"))
  :in-order-to ((test-op (test-op "churchcrm-bulletin-info/tests"))))

(defsystem "churchcrm-bulletin-info/tests"
  :depends-on ("churchcrm-bulletin-info"
	       "fiveam")
  :components ((:file "t/packages")
	       (:file "t/churchcrm-bulletin-info"))
  :perform (test-op (o s)
		    (uiop:symbol-call :churchcrm-bulletin-info-tests
				      :all-tests)))
